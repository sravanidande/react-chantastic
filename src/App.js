import React from "react";

// const collection = [{ name: "react" }, { name: "js" }, { name: "ts" }];

const Pokemon = ({ name, ...props }) => {
  return <h1 {...props}>{name}</h1>;
};

export const usePokemon = index => {
  const [pokemon, setPokemon] = React.useState(null);
  React.useEffect(() => {
    fetch(`https://pokeapi.co/api/v2/pokemon/${index}`)
      .then(res => res.json())
      .then(data => setPokemon(data))
      .catch(new Error());
  }, [index]);
  return pokemon;
};

function App() {
  const [index, setIndex] = React.useState(1);
  let pokemon = usePokemon(index);
  let collection = usePokemon("");

  return (
    <>
      <button onClick={() => setIndex(index + 1)}>Next</button>
      {pokemon ? (
        <Pokemon name={pokemon.name} />
      ) : (
        <div>there is no pokemon {index}</div>
      )}
      {collection ? (
        <ul>
          {collection.results.map(pokemon => (
            <li key={pokemon.name}>{pokemon.name}</li>
          ))}
        </ul>
      ) : (
        <div>Fetching data...</div>
      )}
    </>
  );
}

export default App;
